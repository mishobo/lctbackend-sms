package com.compulynx.compas.bal;

import com.compulynx.compas.models.Organization;
import com.compulynx.compas.models.CompasResponse;
import com.compulynx.compas.models.Merchant;

import java.util.List;

public interface OrganizationBal {
	List<Organization> GetOrganizations();
	
	CompasResponse UpdateOrganization (Organization organozation);
	List<Organization> GetOrganizationsByInsurer(String LoggedInUser);
}
