/**
 *  * Download Member Angular Module * Nelson Kimaiga
 */
'use strict';
angular.module('app.downloadMember', []).controller("downloadMemberCtrl", ["$scope", "$filter", "memberInquirySvc", "memStatementSvc", "$memberValid", "$rootScope", "blockUI", "logger", "$location", "memberSvc", "organizationSvc", "$modal", "txnVerificationSvc", function($scope, $filter, memberInquirySvc, memStatementSvc, $memberValid, $rootScope, blockUI, logger, $location, memberSvc, organizationSvc, $modal, txnVerificationSvc) {
	var init;
	$scope.bioView = true;
	$scope.memVerify = false;
	$scope.memVerify = true;
	$scope.showMemDetails = true;
	$scope.memberViewMode = true;
	$scope.loadedMember = {};
	// doctors report logic
	$scope.orgSelected = true;
	$scope.mem = [];
	$scope.service = [];


	var ModalInstanceCtrl = function($scope, $modalInstance, services) {
		$scope.$watch("state", function(newValue, oldValue) {
			if (newValue != oldValue) {
				$modalInstance.dismiss("cancel")
			}
		});
		$scope.cancelModel = function() {
			$modalInstance.dismiss("cancel")
		}

	};


	var ModalInstanceCtrlDoctor = function($scope, $modalInstance, txns) {
		$scope.$watch("state", function(newValue, oldValue) {
			if (newValue != oldValue) {
				$modalInstance.dismiss("cancel")
			}
		});
		$scope.cancelModel = function() {
			$modalInstance.dismiss("cancel")
		}

	};

	$scope.loadMemberData = function() {

		$scope.members = [], $scope.vouchers = [], $scope.services = [], $scope.searchKeywords = "", $scope.filteredMembers = [], $scope.row = "", $scope.memberInquiryEditMode = false;
		memberSvc.GetMembers().success(function(response) {
			// console.log('inquiry member: ', response);
			return $scope.members = response, $scope.searchKeywords = "", $scope.filteredMembers = [], $scope.row = "", $scope.select = function(page) {
				var end, start;
				return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageMembers = $scope.filteredMembers.slice(start, end)
			}, $scope.onFilterChange = function() {
				return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
			}, $scope.onNumPerPageChange = function() {
				return $scope.select(1), $scope.currentPage = 1
			}, $scope.onOrderChange = function() {
				return $scope.select(1), $scope.currentPage = 1
			}, $scope.search = function() {
				return $scope.filteredMembers = $filter("filter")($scope.members, $scope.searchKeywords), $scope.onFilterChange()
			}, $scope.order = function(rowName) {
				return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredMembers = $filter("orderBy")($scope.members, rowName), $scope.onOrderChange()) : void 0
			}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageMembers = [], (init = function() {
				return $scope.search(), $scope.select($scope.currentPage)
			})();
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
		});

	}




	$scope.onOrgChange = function(orgId) {
		if (orgId > 0) {
			$scope.memberEditMode = false;

		}
	}
	$scope.loadOrganizationData = function() {
		$scope.orgs = [];
		organizationSvc.GetOrganizations().success(function(response) {
			return $scope.orgs = response
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
		});
	}
	$scope.loadOrganizationData();

	$scope.viewMember = function(memberNo) {
		var member = {}
		memberSvc.GetMembers(memberNo).success(function(response) {
			member = response;
			console.log(member);
			if (!member.memberNo) {
				logger.logWarning("Member not registered");
			} else if (member.relation != 0) {
				logger.logWarning("Member is dependent of " + member.principle)
				$scope.showSearch = true;
			} else {
				$scope.memberEditMode = true;
				$scope.memberDetailMode = false;
				$scope.memberViewMode = false;
				$scope.memberId = member.memberId;
				$scope.accNo = member.accNo;
				$scope.memberNo = member.memberNo;
				$scope.category = member.programmeDesc;
				$scope.insuranceCode = member.insuranceCode;
				$scope.schemeCode = member.schemeCode;
				$scope.scheme = member.scheme;
				$scope.memberType = member.memberType;
				$scope.memberTypeValue = "N/A";
				if ($scope.memberType === "P") {
					$scope.memberTypeValue = "Principal";
				} else if ($scope.memberType === "D") {
					$scope.memberTypeValue = "Dependant";
				}
				$scope.outpatientStatus = member.outpatientStatus;
				$scope.orgSelect = member.orgId;
				$scope.showCamera = false;
				$scope.capPic = true;
				$scope.showImage = true;
				$scope.surName = member.surName;
				$scope.title = member.title;
				$scope.firstName = member.firstName;
				// $scope.vouchers=[];
				$scope.otherName = member.otherName;
				$scope.idPassPortNo = member.idPassPortNo;
				$scope.newValues = [];
				$scope.basketVouchers = [];
				$scope.programmes = member.programmes;
				for (var i = 0; i < (member.programmes).length; i++) {
					console.log(member.programmes[i]);
					for (var y = 0; y < ((member.programmes[i].membervouchers).length); y++) {
						$scope.basketVouchers.push(member.programmes[i].membervouchers[y]);
						// looping to services
						for (var j = 0; j < ((member.programmes[i].membervouchers[y].services).length); j++) {
							$scope.newValues.push(member.programmes[i].membervouchers[y].services[j]);
						}

					}
				}
				console.log($scope.newValues);

				var ken = $scope.newValues;
				for (var i = 0; i < ken.length; i++) {
					console.log(ken[i].serviceName);
				}
				console.log(ken);

				$scope.vouchers = member.vouchers;
				// $scope.services = member.services;
				console.log(JSON.stringify(member.programmes[0].membervouchers));

				console.log($scope.basketVouchers);

				var eclaimvouchers = $scope.basketVouchers;
				for (var i = 0; i < eclaimvouchers.length; i++) {
					console.log(eclaimvouchers[i].voucherDesc);
				}
				console.log(eclaimvouchers);


				if (member.gender == "M") {
					$scope.gender = "Male";
					$scope.male = true;
				} else if (member.gender == "F") {
					$scope.gender = "Female";
					$scope.feMale = true;
				}
				$scope.dateOfBirth = member.dateOfBirth;
				$scope.maritalStatus = member.maritalStatus;
				$scope.weight = member.weight;
				$scope.height = member.height;
				$scope.nhifNo = member.nhifNo;
				$scope.employerName = member.employerName;
				$scope.occupation = member.occupation;
				$scope.nationality = member.nationality;
				$scope.postalAdd = member.postalAdd;
				$scope.physicalAdd = member.physicalAdd;
				$scope.homeTelephone = member.homeTelephone;
				$scope.officeTelephone = member.officeTelephone;
				$scope.cellPhone = member.cellPhone;
				$scope.email = member.email;
				$scope.nokName = member.nokName;
				$scope.relationSelect = member.relationId;
				$scope.relationDesc = member.relationDesc;
				$scope.nokIdPpNo = member.nokIdPpNo;
				$scope.nokTelephoneNo = member.nokTelephoneNo;
				$scope.nokPostalAdd = member.nokPostalAdd;
				$scope.myCroppedImage = member.memberPic;
				$scope.coverSelect = member.coverId;
				$scope.coverName = member.coverName;
				$scope.categorySelect = member.categoryId;
				$scope.categoryName = member.categoryName;
				$scope.fullName = member.fullName;
				$scope.ipLimit = member.ipLimit;
				$scope.opLimit = member.opLimit;
				$scope.cams = 0;
				$scope.productSelect = member.productId;
				$scope.showProgrammes = false
				$scope.cardNumber = member.cardNumber
				$scope.categories = member.programmes;
				$scope.bioId = member.bioId;
				$scope.active = member.active;
				if ($scope.bioId == 0) {
					$scope.showEnroll = false;
				} else {
					$scope.showEnroll = true;
				}
				$scope.famMemEntries = member.familyMemList;
				$scope.familySize = member.familySize;
				$scope.isDisabled = true;

				//                 $scope.loadedMember["rec_id"] =
					// member.memberId;
				$scope.loadedMember["scheme_name"] = member.scheme;
				$scope.loadedMember["membership_number"] = member.memberNo;
				console.log("loaded member number is: :" + member.memberNo)
				const totalNames = member.fullName.split(" ").length;
				if (member.firstName) {
					$scope.loadedMember["first_name"] = member.firstName;
				} else if (member.fullName) {
					$scope.loadedMember["first_name"] = member.fullName.split(" ")[0];
				}

				if (member.lastName) {
					$scope.loadedMember["last_name"] = member.lastName;
				} else if (member.surName) {
					$scope.loadedMember["last_name"] = member.surName;
				} else if (member.fullName) {
					$scope.loadedMember["last_name"] = member.fullName.split(" ")[totalNames - 1];
				}

				$scope.loadedMember["middle_name"] = member.otherName;
				// $scope.loadedMember["last_name"] = member.surName;
				$scope.loadedMember["full_name"] = member.fullName;
				$scope.loadedMember["outpatient_status"] = member.outpatientStatus;
				$scope.loadedMember["member_type"] = member.memberType;
				// $scope.loadedMember["insurance_id"] = member.surName;
				// $scope.loadedMember["principal_relationship"] = member.;
				$scope.loadedMember["insurance_id"] = member.insuranceCode;
				$scope.loadedMember["scheme_code"] = member.schemeCode;
				$scope.loadedMember["acc_no"] = member.accNumber;
				$scope.loadedMember["programme_id"] = member.programmes.programmeId;
				$scope.loadedMember["ID"] = member.memberId;
				console.log("loadedMember: ", $scope.loadedMember);


			}
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
		});



	};


	$scope.cancelMember = function() {
		$scope.memberInquiryEditMode = false;
		$scope.userClassId = classId;
		$scope.memberId = 0;
		$scope.surName = "";
		console.log("cancelled");
		$scope.memVerify = false;
		$scope.showMemDetails = true;

		$scope.isDisabled = false;
	}

	$scope.cancelInq = function() {
		$scope.memberInquiryEditMode = false;

		$scope.memberViewMode = true;

	}

	$scope.sendMemberInfo = function() {
		memberInquirySvc.sendInfo($scope.loadedMember).success(function(resp) {
			console.log('sent successfully...', resp);
			logger.logSuccess("Member Details sent sucCessfully to AKUH")
		}).error(function(data, status, headers, config) {
			console.log("error: ", data);
		});
	}

	// MODAL POP-UP FORM
	$scope.billPatient = function(service) {
		console.log('Member service balance UPON CLICKING::' + service.coverBalance)
		if (service.coverBalance == 0 || service.coverBalance < 0) {
			logger.logError("Member Bakset Balance Depleted")
			return false;
		}
		var services = {}
		var selectedBasket = service.serviceName
		console.log("SELECTED BASKET:: " + selectedBasket);
		$scope.contactname = "";
		$scope.service = {};
		$scope.service = service;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/billing.html',
			controller: ModalInstanceCtrl,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				services: function() {

					return $scope.service
				}

			}

		});


	};



	$scope.serviceCategory = ["General Practitioner", "Gynaecology", "pediatrician", "Physiotherapy", "Others"];

	// e-claim first-billing process function
	$scope.BillMember = function(service) {
		var totalRunningBalance;
		console.log('Member service balance before billing ::' + service.coverBalance)
		console.log("loaded Member Number during during billing: ", $scope.loadedMember.membership_number);
		console.log("loaded Member Name during during billing: ", $scope.loadedMember.full_name);
		console.log("loaded Scheme Name during during billing: ", $scope.loadedMember.scheme_name);
		console.log("loaded Member Id during during billing: ", $scope.loadedMember.ID);
		const LoggedInProvider = $rootScope.UsrRghts.hospitalName;
		if (service.serName == null) {
			logger.logWarning("Error! You may have skipped specifying the service to bill")
			return false;
		} else if (service.serviceCategory == null) {
			logger.logWarning("Error! You may have skipped specifying the service category")
			return false;
		} else if (service.totalAmount == null) {
			logger.logWarning("Error! You may have skipped specifying the service amount")
			return false;
		} else if (service.invoiceNo == null) {
			logger.logWarning("Error! You may have skipped inputing the invoice number")
			return false;
		} else {
			var eclaim = {
					serName: service.serName,
					serviceCategory: service.serviceCategory,
					basketId: service.basketId,
					serviceId: service.serviceId,
					MemberId: $scope.loadedMember.ID,
					totalAmount: service.totalAmount,
					merName: $rootScope.UsrRghts.hospitalName,
					invoiceNo: service.invoiceNo,
					createdBy: $rootScope.UsrRghts.sessionId,
					totalRunningBalance: service.coverBalance - service.totalAmount,
					memberNo: $scope.loadedMember.membership_number,
					schemeName: $scope.loadedMember.scheme_name,
					memberName: $scope.loadedMember.full_name,
					basketName: service.serviceName,
					accNo: $scope.loadedMember.acc_no
			};
			console.log('submitted service name::' + service.serName)
			console.log('Provider submiting e-claim', $rootScope.UsrRghts.hospitalName)
			console.log("typeof", LoggedInProvider)
			blockUI.start()
			memberInquirySvc.PostEclaimTransaction(eclaim).success(function(response) {
				if (response.respCode == 200) {
					logger.logSuccess("Great! The Transaction completed succesfully");
					$location.path('/dashboard');
					service.serName = "";
					service.totalAmount = "";
					service.invoiceNo = "";
					$scope.billPatient = false;
					$scope.state += 1;
				} else {
					logger.logWarning(response.respMessage);
				}
				blockUI.stop();
			}).error(function(data, status, headers, config) {
				logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
				blockUI.stop();
			});

		}
	}

	// doctors report
	$scope.previewEclaimTrans = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in provider for doctor:: ", LoggedInHospital)
		$scope.getEclaimTransDetails(LoggedInHospital);
	}

	$scope.getEclaimTransDetails = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		// $scope.serviceCategory = service.serviceCategory;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		txn.serviceCategory = $scope.service.serviceCategory;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.GetEclaimTransaction(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// doctors-report pop-up form
	$scope.viewTransDtl = function(merchants) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = merchants;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/doctors-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};

	// ICD10 diagnosis Array
	$scope.diagnosis = ["General Practitioner", "ENT", "Dentist", "Dermatologist", "Internist", "Obstetric/Gynaecologist", "Pediatrician", "Pharmacist", "Opthamologist", "Surgeon", "Psychiatrist"];

	// submit doctor report
	$scope.submitDoctorReport = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		if (txn.diagnosisType == null) {
			logger.logWarning("Error! You may have skipped selecting the ICD10 Information")
			return false;
		} else if (txn.diagnosisInformation == null) {
			logger.logWarning("Error! You may have skipped specifying the Diagnosis Information")
			return false;
		} else if (txn.doctorsNotes == null) {
			logger.logWarning("Error! You may have skipped inputing the doctor's notes")
			return false;
		} else {
			var eclaim = {
					invoiceNo: txn.invoiceNo,
					diagnosisType: txn.diagnosisType,
					diagnosisInformation: txn.diagnosisInformation,
					doctorNotes: txn.doctorsNotes,
					NeedLabResults: 1,
					labTests: txn.SpecifiedLabTests,
					seenDoctor: 1
			};
			console.log("e-claim transction ID: ", txn.id)
			blockUI.start()
			memberInquirySvc.UpdateEclaimTransaction(eclaim).success(function(response) {
				if (response.respCode == 200) {
					logger.logSuccess("Great! The Doctor's report was succesfully submitted");
					$location.path('/dashboard');
					txn.diagnosisType = "";
					txn.diagnosisInformation = "";
					txn.doctorsNotes = "";
					txn.labServices = "";
					$scope.viewTransDtl = false;
					$scope.state += 1;
				} else {
					logger.logWarning(response.respMessage);
				}
				blockUI.stop();
			}).error(function(data, status, headers, config) {
				logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
				blockUI.stop();
			});

		}
	}

	// lab report
	$scope.previewLabEclaimTrans = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in provider for doctor:: ", LoggedInHospital)
		$scope.getLabEclaimTransDetailss(LoggedInHospital);
	}

	$scope.getLabEclaimTransDetailss = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getLabEclaimTransDetailss(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// lab-report pop-up form
	$scope.viewLabTransDtl = function(merchants) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = merchants;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/lab-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit lab report
	$scope.submitLabReport = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		if (txn.labServices == null) {
			logger.logWarning("Error! You may have skipped specifying the Lab tests Information")
			return false;
		} else if (txn.labAmount == null) {
			logger.logWarning("Error! You may have skipped specifying the Amount to bill")
			return false;
		} else {
			var eclaim = {
					invoiceNo: txn.invoiceNo,
					labResults: txn.labServices,
					labAmount: txn.labAmount,
					NeedLabResults: 0,
					seenLab: 1
			};
			console.log("e-claim transction ID: ", txn.id)
			blockUI.start()
			memberInquirySvc.UpdateLabEclaimTransaction(eclaim).success(function(response) {
				if (response.respCode == 200) {
					logger.logSuccess("Great! The Lab report was succesfully submitted");
					$location.path('/dashboard');
					txn.diagnosisType = "";
					txn.diagnosisInformation = "";
					txn.doctorsNotes = "";
					txn.labServices = "";
					$scope.viewLabTransDtl = false;
					$scope.state += 1;
				} else {
					logger.logWarning(response.respMessage);
				}
				blockUI.stop();
			}).error(function(data, status, headers, config) {
				logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
				blockUI.stop();
			});

		}
	}

	// previewPharmacyTrans
	$scope.previewPharmacyEclaimTrans = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in provider for pharmacy:: ", LoggedInHospital)
		$scope.getPharmacyEclaimTransDetails(LoggedInHospital);
	}


	$scope.getPharmacyEclaimTransDetails = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getPharmacyEclaimTransDetails(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// pharmacy-report pop-up form
	$scope.viewPharmacyTransDtl = function(merchants) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = merchants;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/pharmacy-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit pharmacy report
	$scope.submitPharmacyReport = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		if (txn.drugName == null) {
			logger.logWarning("Error! You may have skipped specifying the Drug name")
			return false;
		} else if (txn.drugQuantity == null) {
			logger.logWarning("Error! You may have skipped specifying the Drug Quantity")
			return false;
		} else if (txn.amountBilled == null) {
			logger.logWarning("Error! You may have skipped specifying the amount to bill")
			return false;
		} else {
			var eclaim = {
					seenPharmacy: 1,
					drugName: txn.drugName,
					drugQuantity: txn.drugQuantity,
					amountBilled: txn.amountBilled,
					// drugInvoice: txn.DruginvoiceNumber,
					invoiceNo: txn.invoiceNo
			};
			console.log("e-claim transction ID: ", txn.id)
			blockUI.start()
			memberInquirySvc.UpdatePharmacyEclaimTransaction(eclaim).success(function(response) {
				if (response.respCode == 200) {
					logger.logSuccess("Great! The Pharmacy report was succesfully submitted");
					$location.path('/dashboard');
					txn.drugName = "";
					txn.diagnosisInformation = "";
					txn.drugQuantity = "";
					txn.invoiceNumber = "";
					$scope.viewPharmacyTransDtl = false;
					$scope.state += 1;
				} else {
					logger.logWarning(response.respMessage);
				}
				blockUI.stop();
			}).error(function(data, status, headers, config) {
				logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
				blockUI.stop();
			});

		}
	}


	// LAB BILLING
	$scope.previewLabEclaimBills = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in provider for lab assistant:: ", LoggedInHospital)
		$scope.getLabEclaimBillDetails(LoggedInHospital);
	}

	$scope.getLabEclaimBillDetails = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getLabEclaimBillDetails(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// lab-bill-report pop-up form
	$scope.viewLabBillDtl = function(txn) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = txn;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/lab-bill-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit lab bill
	$scope.submitLabBill = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		console.log("Is there a balance here?: ", txn.totalRunningBalance);
		console.log("loaded member acc_no during during billing: ", txn.accNo);
		console.log("loaded Member Id during during billing: ", txn.MemberId);
		var eclaim = {
				billedLab: 1,
				totalRunningBalance: txn.totalRunningBalance - txn.labAmount,
				invoiceNo: txn.invoiceNo,
				basketId: txn.basketId,
				serviceId: txn.serviceId,
				MemberId: txn.MemberId,
				accNo: txn.accNo
		};
		blockUI.start()
		memberInquirySvc.UpdateLabEclaimBillTransaction(eclaim).success(function(response) {
			if (response.respCode == 200) {
				logger.logSuccess("Great! The Lab transction was succesfully completed");
				$location.path('/dashboard');
				txn.diagnosisType = "";
				txn.diagnosisInformation = "";
				txn.doctorsNotes = "";
				txn.labServices = "";
				$scope.viewLabBillDtl = false;
				$scope.state += 1;
			} else {
				logger.logWarning(response.respMessage);
			}
			blockUI.stop();
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
			blockUI.stop();
		});
	}



	// pharmacy BILLING
	$scope.previewPharmacyEclaimBills = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in pharmacist :: ", LoggedInHospital)
		$scope.getPharmacyEclaimBillDetails(LoggedInHospital);
	}

	$scope.getPharmacyEclaimBillDetails = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getPharmacyEclaimBillDetails(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// pharmacy-bill-report pop-up form
	$scope.viewPharmacyBillDtl = function(txn) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = txn;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/pharmacy-bill-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit pharmacy bill
	$scope.submitPharmacyBill = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		console.log("e-claim account number when submiting: ", txn.accNo)
		console.log("Is there a balance here?: ", txn.totalRunningBalance)
		if (txn.totalRunningBalance == 0 || txn.totalRunningBalance < 0) {
			logger.logError("Member's Balance Has been Depleted")
			return false;
		}

		var eclaim = {
				billedPharmacy: 1,
				totalRunningBalance: txn.totalRunningBalance - txn.amountBilled,
				invoiceNo: txn.invoiceNo,
				basketId: txn.basketId,
				serviceId: txn.serviceId,
				MemberId: txn.MemberId,
				accNo: txn.accNo
		};
		blockUI.start()
		memberInquirySvc.UpdatePharmacyEclaimBillTransaction(eclaim).success(function(response) {
			if (response.respCode == 200) {
				logger.logSuccess("Great! The Pharmacy Transaction was succesfully completed");
				$location.path('/dashboard');
				txn.diagnosisType = "";
				txn.diagnosisInformation = "";
				txn.doctorsNotes = "";
				txn.labServices = "";
				$scope.viewPharmacyBillDtl = false;
				$scope.state += 1;
			} else {
				logger.logWarning(response.respMessage);
			}
			blockUI.stop();
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
			blockUI.stop();
		});
	}


	// FINAL E-CLAIM SUBMISSION
	$scope.previewEclaimForm = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in Cashier :: ", LoggedInHospital)
		$scope.getEclaimDetails(LoggedInHospital);
	}

	$scope.getEclaimDetails = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getEclaimDetails(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// submit E-claim pop-up form
	$scope.viewEclaimDtl = function(txn) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = txn;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/submit-eclaim-modal.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit eclaim info
	$scope.submitEclaim = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		var eclaim = {
			eclaimSubmitted: 1,
			invoiceNo: txn.invoiceNo
		};
		blockUI.start()
		memberInquirySvc.UpdateEclaimStatus(eclaim).success(function(response) {
			if (response.respCode == 200) {
				logger.logSuccess("Completed! The E-claim Information for invoice : " +txn.invoiceNo+  " was succesfully submitted");
				$location.path('/eclaim/submit-eclaim');
				$scope.viewEclaimDtl = false;
				$scope.state += 1;
			} else {
				logger.logWarning(response.respMessage);
			}
			blockUI.stop();
		}).error(function(data, status, headers, config) {
			logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
			blockUI.stop();
		});
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// after lab tests
	$scope.previewTests = function(mem) {
		const LoggedInHospital = $rootScope.UsrRghts.hospitalName;
		console.log("E-claim logged in provider for doctor:: ", LoggedInHospital)
		$scope.getLabResults(LoggedInHospital);
	}

	$scope.getLabResults = function(LoggedInHospital) {
		$scope.trans = [];
		var txnnstatus = '0';
		var txn = {}
		$scope.merName = $rootScope.UsrRghts.hospitalName;
		$scope.fromDt = $filter('date')($scope.mem.FromDt, 'yyyy-MM-dd');
		$scope.toDt = $filter('date')($scope.mem.ToDt, 'yyyy-MM-dd');
		txn.fromDt = $scope.fromDt;
		txn.toDt = $scope.toDt;
		txn.merName = $scope.merName;
		if ($scope.fromDt == null) {
			logger.logWarning("Opss! You may have skipped specifying from date")
			return false;
		} else if ($scope.toDt == null) {
			logger.logWarning("Opss! You may have skipped specifying to date")
			return false;
		} else if (txn.fromDt > txn.toDt) {
			logger.logWarning("Oops! From date cannot be later than to date.")
			return false;
		} else {
			txn.txnStatus = txnnstatus;
			memberInquirySvc.getLabResults(txn).success(function(response) {
				$scope.showBillDetails = false;
				return $scope.trans = response,
				$scope.searchKeywords = "", $scope.filteredTrans = [], $scope.row = "", $scope.select = function(page) {
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageTrans = $scope.filteredTrans.slice(start, end)
				}, $scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1
				}, $scope.search = function() {
					return $scope.filteredTrans = $filter("filter")($scope.trans, $scope.searchKeywords), $scope.onFilterChange()
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredTrans = $filter("orderBy")($scope.trans, rowName), $scope.onOrderChange()) : void 0
				}, $scope.numPerPageOpt = [3, 5, 10, 20], $scope.numPerPage = $scope.numPerPageOpt[2], $scope.currentPage = 1, $scope.currentPageTrans = [], (init = function() {
					return $scope.search(), $scope.select($scope.currentPage)
				})();
			});
		}
	}

	// lab result pop-up
	$scope.viewLabResultsDtl = function(merchants) {
		var txns = {}
		$scope.contactname = "";
		$scope.txn = {};
		$scope.txn = merchants;
		$scope.modalInstance = $modal.open({
			templateUrl: 'views/eclaims/lab-report.html',
			controller: ModalInstanceCtrlDoctor,
			backdrop: 'static',
			scope: $scope,
			resolve: {
				txns: function() {

					return $scope.txn
				}

			}

		});


	};


	// submit lab report
	$scope.submitLabReport = function(txn) {
		console.log("e-claim invoice_number when submiting: ", txn.invoiceNo)
		if (txn.labServices == null) {
			logger.logWarning("Error! You may have skipped specifying the Lab tests Information")
			return false;
		} else if (txn.labAmount == null) {
			logger.logWarning("Error! You may have skipped specifying the Amount to bill")
			return false;
		} else {
			var eclaim = {
					invoiceNo: txn.invoiceNo,
					labResults: txn.labServices,
					labAmount: txn.labAmount,
					NeedLabResults: 0,
					seenLab: 1
			};
			console.log("e-claim transction ID: ", txn.id)
			blockUI.start()
			memberInquirySvc.AddPrescription(eclaim).success(function(response) {
				if (response.respCode == 200) {
					logger.logSuccess("Great! The Lab report was succesfully submitted");
					$location.path('/dashboard');
					txn.diagnosisType = "";
					txn.diagnosisInformation = "";
					txn.doctorsNotes = "";
					txn.labServices = "";
					$scope.viewLabTransDtl = false;
					$scope.state += 1;
				} else {
					logger.logWarning(response.respMessage);
				}
				blockUI.stop();
			}).error(function(data, status, headers, config) {
				logger.logError("Oh snap! There is a problem with the server, please contact the adminstrator.")
				blockUI.stop();
			});

		}
	}


}]).factory('memberInquirySvc', function($http) {
	var shpMemberInquirySvc = {};
	shpMemberInquirySvc.PostEclaimTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/insertEclaimTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};
	
	// after fetching lab results
	shpMemberInquirySvc.getLabResults = function(txn) {
		return $http({
			url: '/compas/rest/user/getLabResults/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};
	

	// ECLAIM SUBMISSION
	shpMemberInquirySvc.getEclaimDetails = function(txn) {
		return $http({
			url: '/compas/rest/user/getEclaimDetails/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};

	shpMemberInquirySvc.UpdateEclaimStatus= function(eclaim) {
		return $http({
			url: '/compas/rest/user/UpdateEclaimStatus',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	// pharmacy billing
	shpMemberInquirySvc.getPharmacyEclaimBillDetails = function(txn) {
		return $http({
			url: '/compas/rest/user/getPharmacyEclaimBills/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};


	shpMemberInquirySvc.UpdatePharmacyEclaimBillTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/updPharmacyEclaimBillTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	shpMemberInquirySvc.UpdateEclaimTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/updEclaimTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	shpMemberInquirySvc.UpdateLabEclaimTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/updLabEclaimTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	shpMemberInquirySvc.UpdateLabEclaimBillTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/updLabEclaimBillTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	shpMemberInquirySvc.UpdatePharmacyEclaimTransaction = function(eclaim) {
		return $http({
			url: '/compas/rest/user/updPharmacyEclaimTrans',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: eclaim
		});
	};

	shpMemberInquirySvc.GetEclaimTransaction = function(txn) {
		return $http({
			url: '/compas/rest/user/getEclaimTrans/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};

	shpMemberInquirySvc.getLabEclaimTransDetailss = function(txn) {
		return $http({
			url: '/compas/rest/user/getLabEclaimTrans/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};

	shpMemberInquirySvc.getLabEclaimBillDetails = function(txn) {
		return $http({
			url: '/compas/rest/user/getLabEclaimBills/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};

	shpMemberInquirySvc.getPharmacyEclaimTransDetails = function(txn) {
		return $http({
			url: '/compas/rest/user/getPharmacyEclaimTrans/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			data: txn
		});
	};

	shpMemberInquirySvc.GetSingleMember = function(memberNo) {
		return $http({
			url: '/compas/rest/member/gtsingleMember?memberNo=' + encodeURIComponent(memberNo),
			method: 'GET'

		});
	};
	shpMemberInquirySvc.GetMemberBio = function(bioId) {
		return $http({
			url: '/compas/rest/member/gtbioMember?bioId=' + encodeURIComponent(bioId),
			method: 'GET'

		});

	};
	shpMemberInquirySvc.sendInfo = function(loadedMember) {
		console.log('posting to AKUH: ', JSON.stringify(loadedMember));
		return $http({
			url: 'http://41.139.212.137:8085/compas/lct/akuh/memberVerification/save_verified_lct_member',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			withCredentials: true,
			data: loadedMember

		});
	};

	return shpMemberInquirySvc;
}).factory('$memberValid', function() {
	return function(valData) {
		if (angular.isUndefined(valData))
			return false;
		else {
			if (valData == null)
				return false;
			else {
				if (valData.toString().trim() == "")
					return false;
				else
					return true;
			}
		}
		return false;
	};
});