package com.compulynx.compas.models;

public class EclaimTransactionDtl {
	public int id;
	public int transMstId;
	public int voucherId;
	public String voucherName;
	public int serviceId;
	public double txnAmount;
	public double runningBalance;
	public String transDate;
	public int userId;
	public int createdBy;
	public int respCode;
	public String comment;
	public String transStatus;
	public int transAuthUser;
	public int transDetailId;
	public String transCancelReason;
	public String serName;
	public String image;
	public String accNo;
	public int programmeId;
	public int txnId;
	public String schemeName;
	public String memberName;
	public String invoiceNo;
	public Object memberNo;
	public String merName;
	public String basketName;
	public int basketId;
	public String serviceCategory;
	public double labAmount;
	public double totalRunningBalance;
	public double amountBilled;
	public double eclaimTotalAmount;
	public int memberId;
	public String labTests;
}
